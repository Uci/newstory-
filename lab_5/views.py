from django.shortcuts import render, redirect
from .forms import ScheduleForm, RegisterForm
from .models import Schedule, Register
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt

from django.core.validators import validate_email
from django.core.exceptions import ValidationError
import json


def mainHome(request):
 	return render(request,'pepew3.html')

def profile(request):
	return render(request, 'pepew3_2.html')

def register(request):
	response = {}
	response['registrasi'] = RegisterForm 
	return render(request, 'pepew3_3.html', response)


def register_form(request):
	response = {}
	form = RegisterForm(request.POST or None)
	if(request.method == "POST" and form.is_valid()):
		name = request.POST.get('name')
		email = request.POST.get('email')
		password = request.POST.get('password')

		Register.objects.create(name=name, email=email, password=password)
		return JsonResponse({'result' : 'Thanks ' + name + ' for subscribe me!!!'})

	else:
		return JsonResponse({'result' : 'fail'})


def email_validation(request):
	try:
		validate_email(request.POST.get('email'))
	except:
		return JsonResponse({
			'message':'Email format is invalid!',
			'status':'fail'
		})

	exist = Register.objects.filter(email=request.POST['email'])

	if exist:
		return JsonResponse({
			'message':'Email already exists!',
			'status':'fail'
		})

	return JsonResponse({
		'message':'Email now valid',
		'status':'success'
	})


def schedule_form(request):
	response = {}
	response['form'] = ScheduleForm
	return render(request, "ScheduleForm.html", response)


def display_form(request):	
	response = {}
	form = ScheduleForm(request.POST or None)
	if(request.method == "POST" and form.is_valid()):
		response['day'] = request.POST.get('day')
		response['date'] = request.POST.get('date')
		response['hour_min'] = request.POST.get('hour_min')
		response['activity'] = request.POST.get('activity')
		response['place'] = request.POST.get('place')
		response['category'] = request.POST.get('category')

		Schedules = Schedule(day=response['day'], date=response['date'], hour_min=response['hour_min'],
		 					 activity=response['activity'], place=response['place'], category=response['category'])

		
		Schedules.save()

		response['form'] = form

		Schedules = Schedule.objects.all()
		response['Schedules'] = Schedules
		return render(request, 'ScheduleList.html' , response)
	
	else:
		return HttpResponseRedirect('/')

def schedule_list(request):
	response = {}
	Schedules = Schedule.objects.all()
	response['Schedules'] = Schedules
	return render(request, 'ScheduleList.html' , response)

def remove_schedule(request):
	Schedules = Schedule.objects.all().delete()
	return render(request, 'ScheduleList.html', {})

def register_list(request):
	response = []
	# count = 1;
	register = Register.objects.all()
	for i in register:
		response.append({'name':i.name, 'email':i.email, 'pass':i.password})
		# count += 1

	data = response[::-1]
	return JsonResponse({
		'dataa' : data
 		})

def unsubscribe(request):
	email = request.GET['email']
	email = email[1:-1]
	Register.objects.filter(email= email).delete()

	response = []
	# count = 1;
	register = Register.objects.all()
	for i in register:
		response.append({'name':i.name, 'email':i.email, 'pass':i.password})
		# count += 1

	data = response[::-1]
	return JsonResponse({
		'dataa' : data
 		})