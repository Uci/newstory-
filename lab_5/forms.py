from django import forms
import datetime

class ScheduleForm(forms.Form):
	day = forms.CharField(label="Day")
	date = forms.DateTimeField(label="Date",widget = forms.DateInput(attrs = {'type' : 'date'}))
	hour_min = forms.TimeField(label="Hour : Min", widget = forms.TimeInput(attrs={'type' : 'time'}))
	activity = forms.CharField(label="Activity")
	place = forms.CharField(label="Location")
	category = forms.CharField(label="Category")

class RegisterForm(forms.Form):
	name = forms.CharField(label="Name", required=True, widget=forms.TextInput(attrs={'id' : 'subscriberName'}))
	email = forms.EmailField(label="Email", required=True, widget=forms.TextInput(attrs={'id' : 'subscriberEmail'}))
	password = forms.CharField(label="Password", required=True, min_length=8, max_length=15,
								 widget=forms.PasswordInput(attrs={'id' : 'subscriberPass'}))


	