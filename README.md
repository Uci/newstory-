# My New Story
* Nama		: Gusti Ngurah Yama Adi Putra
* NPM		: 1706979253
* Kelas 	: B

## Herokuapp Link
* https://orangsibuk.herokuapp.com (Main Web)
* https://orangsibuk.herokuapp.com/lab_8 (Acordion, Change Theme, and Make Status)
* https://orangsibuk.herokuapp.com/lab_9 (Library and Favourite, OAuth)

## Pipeline Status
[![pipeline status](https://gitlab.com/Uci/newstory-/badges/master/pipeline.svg)](https://gitlab.com/Uci/newstory-/commits/master)

## Coverage Report
[![coverage report](https://gitlab.com/Uci/newstory-/badges/master/coverage.svg)](https://gitlab.com/Uci/newstory-/commits/master)