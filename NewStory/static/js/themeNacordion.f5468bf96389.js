function dark(){
	$('link[href="/static/css/style.css"]').attr('href', '/static/css/style2.css');
	var button = $('button[name="change_dark"]');
	button.attr('class', "btn btn-warning");
	button.attr('name', "change_orange");
	button.attr('onclick', "orange()");
	button.text("Ori Theme");
}   

function orange(){
	$('link[href="/static/css/style2.css"]').attr('href', '/static/css/style.css');
	var button = $('button[name="change_orange"]');
	button.attr('class', "btn btn-dark");
	button.attr('name', "change_dark");
	button.attr('onclick', "dark()");
	button.text("Dark Theme");
} 

$(function($) {
    
  $('dd[name="hide"]').hide();
  var allPanels = $('dd');
    
  $('dt a').click(function() {
    allPanels.slideUp();
    $(this).parent().next().slideDown();
    return false;
  });

});    


$("button[name='organisasi']").click(function(){
	$.ajax({
		url:"static/js/organisasi.json",
		success: function(result){
			console.log(result);
			$("#demo").html(result);
		}
	});
});

