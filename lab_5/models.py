from django.db import models
from django.utils import timezone
from datetime import date, datetime

class Schedule(models.Model):
	day = models.CharField(max_length=27)
	date = models.DateTimeField()
	hour_min = models.TimeField(auto_now = False, auto_now_add = False)
	activity = models.CharField(max_length=27)
	place = models.CharField(max_length=27)
	category = models.CharField(max_length=27)

class Register(models.Model):
	name = models.CharField(max_length=30)
	email = models.EmailField(max_length=40, unique=True)
	password = models.CharField(max_length=20)
