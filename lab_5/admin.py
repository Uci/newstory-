from django.contrib import admin
from .models import Schedule, Register

admin.site.register(Schedule)
admin.site.register(Register)
# Register your models here.
