var emailIsValid = false;

$(document).ready(function() {
    var table = document.getElementById("boddy");
    table.innerHTML = "";
    $.ajax({
            url: "/subscribe_list/",
            // data: 'json',
            success: function(result){
                results = result.dataa; //dalam array
                console.log(result);
                  
                for(var i in results){
                    var tmp = "<td>" + results[i].name + 
                    "</td><td>" + results[i].email + 
                    "<td><button class='btn btn-sm btn-danger' onclick=unsubscribe(this.id) id="+results[i].email+">Unsubscribe</button></td></tr>"; //onclick=favorite(this.id) 
                    table.insertAdjacentHTML('beforeend', tmp);
                }
            }
        });

    //fungsi ketika di klik di luar input box
    $("subscribe").focusout(function() {
        checkEmail();
        checkAll();
    });
    
    $("#subscriberEmail").keyup(function() {
        checkEmail();
    });
    
    $("#subscriberPass").keyup(function() {
        $('#subscribeStatus').html('');
        if ($('#subscriberPass').val().length < 8) {
            $('#subscribeStatus').append('<small style="color: red"> Password at least 8 character </small>');
        }
        checkAll();
    });

    $('#subscribe').click(function() {
        data = {
            'email' : $('#subscriberEmail').val(),
            'name' : $('#subscriberName').val(),
            'password' : $('#subscriberPass').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        $.ajax({
            type : 'POST',
            url : '/subscribeform/',
            data : data,
            dataType : 'json',
            success : function(data) {
                alert(data['result']);
                document.getElementById('subscriberEmail').value = '';
                document.getElementById('subscriberName').value = '';
                document.getElementById('subscriberPass').value = '';

                $('#subscribeStatus').html('');
                $('#subscribeStatus').append('<small style="color:green">Thanks For Subscribe!</small>');
            }
        });
    });
})

function unsubscribe(email){
    var table = document.getElementById("boddy");
    table.innerHTML = "";
    console.log(email)
     $.ajax({
        url: "/unsubscribe/?email='" + email +"'",
        // data: 'json',
        success: function(result){
            results = result.dataa; //dalam array
            console.log(result);
              
            for(var i in results){
                var tmp = "<td>" + results[i].name + 
                "</td><td>" + results[i].email + 
                "<td><button class='btn btn-sm btn-danger' onclick=unsubscribe(this.id) id="+results[i].email+">Unsubscribe</button></td></tr>"; //onclick=favorite(this.id) 
                table.insertAdjacentHTML('beforeend', tmp);
            }
        }
    });
}

function checkEmail() {
    data = {
        'email':$('#subscriberEmail').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    // console.log('1');
    $.ajax({
        type: "POST",
        url: '/email_validation/',
        data: data,
        dataType: 'json',
        success: function(data) {
            $('#subscribeStatus').html('');
            if (data['status'] === 'fail') {
                emailIsValid = false;
                $('#subscribe').prop('disabled', true);
                $('#subscribeStatus').append('<small style="color:red">' + data["message"] + '</small>');
            } else {
                emailIsValid = true;
                checkAll();
                $('#subscribeStatus').append('<small style="color:green">' + data["message"] + '</small>');
            }
            
        }
    });
}

function checkAll() {
    if (emailIsValid && 
        $('#subscriberName').val() !== '' && 
        $('#subscriberPass').val() !== '' &&
        $('#subscriberPass').val().length > 7) {
        
        $('#subscribe').prop('disabled', false);
    } else {
        $('#subscribe').prop('disabled', true);
    }
}
