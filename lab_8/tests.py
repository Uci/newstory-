from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import makeStatus, remove_list
from .models import Status
from .forms import statusForm

class Lab8Test(TestCase):
	def test_urls_url_is_exist(self):
		#Routing check
		response = Client().get('/lab_8/')
		self.assertEqual(response.status_code,200)

	def test_urls_using_landing_page_template(self):
		response = Client().get('/lab_8/')
		self.assertTemplateUsed(response, 'landing_page.html')

	def test_urls_using_makeStatus_func(self):
		#Views check
		found = resolve('/lab_8/')
		self.assertEqual(found.func, makeStatus)
		
	def test_urls_using_remove_list_func(self):
		#Views check
		find = resolve('/lab_8/remove/')
		self.assertEqual(find.func, remove_list)


	def test_model_can_create_new_status(self):
		#Creating a new status
		new_status = Status.objects.create(status="sedang sibuk")

		#Retrieving all available status
		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status,1)

	def test_models_status_request_post_success(self):
		#Store modes 
		status = "pepew"
		response = Client().post('/lab_8/',{'status': status})
		self.assertEqual(response.status_code, 302)

# from selenium import webdriver
# import unittest
# from selenium.webdriver.chrome.options import Options


# class FuctionalTest(LiveServerTestCase): 
# 	def setUp(self): 
# 		chrome_op = Options()
# 		chrome_op.add_argument('--no-sandbox')
# 		chrome_op.add_argument('--headless')
# 		chrome_op.add_argument('--disable-dev-shm-usage')
		
# 		self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_op)
# 		super(FuctionalTest, self).setUp()
# 		self.browser.implicitly_wait(5)
	
# 	def tearDown(self): 
# 		self.browser.quit() 
# 		super(FuctionalTest, self).tearDown()

# 	def test_can_make_status(self): 
# 		#Open my website
# 		self.browser.get(self.live_server_url + "/lab_8/")
# 		#Check the title
# 		self.assertIn('TDD-D', self.browser.title)

# 		#Make input status
# 		stats_box = self.browser.find_element_by_name('status')
# 		stats_box.send_keys('Coba-coba')
# 		stats_box.submit()

# 		#Check is the status made
# 		self.assertIn('Coba-coba',  self.browser.page_source)

# 		#Click delete button
# 		delete_stats = self.browser.find_element_by_name('delete')
# 		delete_stats.click()

# 		#Check is deleted
# 		self.assertIn('Anda tidak memiliki kenangan...',  self.browser.page_source)

# 	def test_positioning_and_CSS(self):
# 		self.browser.get('https://orangsibuk.herokuapp.com/lab_8/')

# 		#positioning test
# 		h1 = self.browser.find_element_by_tag_name('h1').text 	#is there any tag h1?
# 		h3 = self.browser.find_element_by_tag_name('h3').text 	#is there any tag h3?
# 		self.assertIn('Hello Apa Kabar?', h1)	#is there "Hello Apa Kabar?" in h1?
# 		self.assertIn('Daftar Kenangan', h3)	#is there "Daftar Kenangan" in h1?

# 		#CSS test
# 		body = self.browser.find_element_by_tag_name('body').value_of_css_property("background-color")
# 		self.assertIn('rgba(255, 208, 137, 1)', body)	#Check body background color
# 		text = self.browser.find_element_by_tag_name('table').value_of_css_property('table-layout')
# 		self.assertIn('fixed', text)	#Check table layout

	# def test_change_theme(self):
	# 	self.browser.get('https://orangsibuk.herokuapp.com/lab_8/')
	# 	button = self.browser.find_element_by_name('change_dark')
	# 	button.click();

	# 	body = self.browser.find_element_by_tag_name('body').value_of_css_property("background-color")
	# 	self.assertIn('rgba(35, 35, 35, 1)', body)	#Check body background color
	# 	text = self.browser.find_element_by_class_name('text').value_of_css_property('color')
	# 	self.assertIn('rgba(236, 236, 236, 1)', text)	#Check table layout

	# 	button = self.browser.find_element_by_name('change_orange')
	# 	button.click();

	# 	body = self.browser.find_element_by_tag_name('body').value_of_css_property("background-color")
	# 	self.assertIn('rgba(255, 208, 137, 1)', body)	#Check body background color
	# 	text = self.browser.find_element_by_class_name('text').value_of_css_property('color')
	# 	self.assertIn('rgba(33, 37, 41, 1)', text)	#Check table layout

# if __name__ == '__main__': 
# 	unittest.main(warnings='ignore') 
