from django.test import TestCase, Client
from django.urls import resolve
from .views import *

class Lab8Test(TestCase):
	def test_urls_url_home_is_exist(self):
		#Routing check
		response = Client().get('/lab_9/')
		self.assertEqual(response.status_code,200)

	def test_urls_url_logout_is_exist(self):
		#Routing check
		response = Client().get('/lab_9/logout/')
		self.assertEqual(response.status_code,302)

	def test_urls_url_unfav_is_exist(self):
		#Routing check
		response = Client().get('/lab_9/unfavourite/')
		self.assertEqual(response.status_code,200)

	def test_urls_url_getfav_is_exist(self):
		#Routing check
		response = Client().get('/lab_9/get-fav/')
		self.assertEqual(response.status_code,200)

	def test_urls_url_fav_is_exist(self):
		response = Client().get('/lab_9/fav/')
		self.assertEqual(response.status_code,200)


	def test_urls_using_home_func(self):
		#Views check
		found = resolve('/lab_9/')
		self.assertEqual(found.func, home)

	def test_urls_using_get_data_func(self):
		#Views check
		find = resolve('/lab_9/data/')
		self.assertEqual(find.func, getData)

	def test_urls_using_logout__func(self):
		#Views check
		found = resolve('/lab_9/logout/')
		self.assertEqual(found.func, logout_)

	def test_urls_using_unfav_func(self):
		#Views check
		find = resolve('/lab_9/unfavourite/')
		self.assertEqual(find.func, unfavourite)

	def test_urls_using_fav_func(self):
		#Views check
		found = resolve('/lab_9/fav/')
		self.assertEqual(found.func, fav)



# from selenium import webdriver
# import unittest
# from selenium.webdriver.chrome.options import Options


# class FuctionalTest(LiveServerTestCase): 
# 	def setUp(self): 
# 		chrome_op = Options()
# 		chrome_op.add_argument('--no-sandbox')
# 		chrome_op.add_argument('--headless')
# 		chrome_op.add_argument('--disable-dev-shm-usage')
		
# 		self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_op)
# 		super(FuctionalTest, self).setUp()
# 		self.browser.implicitly_wait(5)
	
# 	def tearDown(self): 
# 		self.browser.quit() 
# 		super(FuctionalTest, self).tearDown()

# 	def test_can_make_status(self): 
# 		#Open my website
# 		self.browser.get(self.live_server_url + "/lab_8/")
# 		#Check the title
# 		self.assertIn('TDD-D', self.browser.title)

# 		#Make input status
# 		stats_box = self.browser.find_element_by_name('status')
# 		stats_box.send_keys('Coba-coba')
# 		stats_box.submit()

# 		#Check is the status made
# 		self.assertIn('Coba-coba',  self.browser.page_source)

# 		#Click delete button
# 		delete_stats = self.browser.find_element_by_name('delete')
# 		delete_stats.click()

# 		#Check is deleted
# 		self.assertIn('Anda tidak memiliki kenangan...',  self.browser.page_source)

# 	def test_positioning_and_CSS(self):
# 		self.browser.get('https://orangsibuk.herokuapp.com/lab_8/')

# 		#positioning test
# 		h1 = self.browser.find_element_by_tag_name('h1').text 	#is there any tag h1?
# 		h3 = self.browser.find_element_by_tag_name('h3').text 	#is there any tag h3?
# 		self.assertIn('Hello Apa Kabar?', h1)	#is there "Hello Apa Kabar?" in h1?
# 		self.assertIn('Daftar Kenangan', h3)	#is there "Daftar Kenangan" in h1?

# 		#CSS test
# 		body = self.browser.find_element_by_tag_name('body').value_of_css_property("background-color")
# 		self.assertIn('rgba(255, 208, 137, 1)', body)	#Check body background color
# 		text = self.browser.find_element_by_tag_name('table').value_of_css_property('table-layout')
# 		self.assertIn('fixed', text)	#Check table layout

	# def test_change_theme(self):
	# 	self.browser.get('https://orangsibuk.herokuapp.com/lab_8/')
	# 	button = self.browser.find_element_by_name('change_dark')
	# 	button.click();

	# 	body = self.browser.find_element_by_tag_name('body').value_of_css_property("background-color")
	# 	self.assertIn('rgba(35, 35, 35, 1)', body)	#Check body background color
	# 	text = self.browser.find_element_by_class_name('text').value_of_css_property('color')
	# 	self.assertIn('rgba(236, 236, 236, 1)', text)	#Check table layout

	# 	button = self.browser.find_element_by_name('change_orange')
	# 	button.click();

	# 	body = self.browser.find_element_by_tag_name('body').value_of_css_property("background-color")
	# 	self.assertIn('rgba(255, 208, 137, 1)', body)	#Check body background color
	# 	text = self.browser.find_element_by_class_name('text').value_of_css_property('color')
	# 	self.assertIn('rgba(33, 37, 41, 1)', text)	#Check table layout

# if __name__ == '__main__': 
# 	unittest.main(warnings='ignore') 
